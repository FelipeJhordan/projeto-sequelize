var router =  require("express").Router();
const controller = require("../../post/controller")

router.post("/", controller.create)

router.get("/", controller.findAll)

router.patch("/", controller.update)

router.delete("/", controller.remove)

module.exports = router
