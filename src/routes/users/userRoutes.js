var routes = require('express').Router()

const controller = require("../../user/controller")

routes.post("/", controller.create)

routes.get("/", controller.findAll)

module.exports = routes