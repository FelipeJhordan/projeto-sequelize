var router = require('express').Router()
// Jeito preguiçoso de fazer 
let projectDir = __dirname.replace("\\src\\routes", "")
router.use("/usuarios", require("./users/userRoutes"))
router.use("/postagens", require("./posts/postRoutes"))

router.get("/", (req, res) => {
    res.sendFile(projectDir + "\\public\\html\\index.html")
})


module.exports = router