const db = require("./../configs/sequelize")
const Post = require("./model")

const User = require("./../user/model")
const { Op } = db.Sequelize

exports.create = (req, res) => {
    console.log(req.body)
    Post.create( {
        content: req.body.content,
        userId: req.body.userId
    }, {
        include:[{
            association: Post.User
        }]
    }).then((post) => {
        res.send(post)
    }).catch((err) => {
        console.log("Erro" + err)
    })
}

exports.findAll = (req, res) => {
    console.log({"ola":req.query.content == ""})
    if(req.query.content == "") {
        console.log({mensagem: "Você chegou aqui"})
            Post.findAll({include: User, order: ['createdAt']}).then( posts => {
                res.send(posts)
         })
    } else {
            Post.findAll({include: User, where: {content: {[Op.like]: "%" + req.query.content + "%"}}, order: ['createdAt']}).then( posts => {
                res.send(posts)
        })
    }
}

exports.update = (req, res) => {
    console.log("Eai")
    Post.update(
        {
            content: req.body.content}, { 
                where: {
                    id: req.body.id 
                }
        }
        ).then( () => {
            res.send({"message": "ok"})
        })
}

exports.remove = (req, res) => {
    Post.destroy( {
        where: {
            id: req.body.id
        }
    } ).then( affectRows => {
        res.send({qtdRowsAffect: affectRows})
    })
}