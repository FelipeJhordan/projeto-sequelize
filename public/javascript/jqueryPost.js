Posts = {
    add : () => {
        var t = {}
        t.content = $("#content").val()
        t.firstname = $("#firstname").val()
        t.lastname = $("#lastname").val()
        
        $.ajax({
            type: 'POST',
            url: 'postagens',
            data: t,
            dataType: 'json',
            success: Posts.template
        })
        Posts.findAll()
        return false
    },

    template: (data) => {
        var comment = $("<div> </div>").attr("id", "comment-" + data.id).attr("class", "comment")
        var content = $("<textarea></textarea>").attr("disabled", true).attr("class", "content").html(data.content)
        var user = $("<p></p>").attr("class", "user")
        if(data.user){
             user.html("Por " + data.user.firstname + " " + data.user.lastname )
        } else {
            user.html("Por "+ $("#user option:selected").text())
        }
        var dtCreation = new Date(data.createdAt)
        dtCreation =  "\t" + (dtCreation.getDate() < 10 ? "0" + dtCreation.getDate() : dtCreation.getDate()) + "/" + ((dtCreation.getMonth() + 1) > 10 ? "0" +dtCreation.getMonth() : dtCreation.getMonth()) + "/"  + dtCreation.getFullYear()
        var date = $("<span></span>").attr("class", "date").html(dtCreation)

        var btnEdit = $("<button> </button>").attr("class", "edit ").html("Editar")
        var btnSave = $("<button> </button>").attr("class", "save hidden").html("Salvar")
        var btnRemove = $("<button> </button>").attr("class", "remove").html("Remover")
        $(btnEdit).on('click', (event) => {
            Posts.enableEdit(event.target)
        })

         $(btnSave).on('click', (event) => {
            Posts.update(event.target)
        })

        $(btnRemove).on('click', (event) => {
            Posts.remove(event.target)
        })

        $(user).append(date)
        $(comment).append(content)
        $(comment).append(user)
        $(comment).append(btnEdit)
        $(comment).append(btnSave)
        $(comment).append(btnRemove)

        $("#comments").append(comment)

    },
    findAll: () => {
        $.ajax({
            type: "GET",
            url: "/postagens",
            data: {content: $("#content-search").val()},
            success: (data) => {
                $("#comments").empty()
                console.log(data)
                for( var post of data) {
                    Posts.template(post)
                }
            },
            error: () => {
                console.log("Ocorreu um erro!")
            },
            dataType: 'json'
        })
    },

    enableEdit : (button) => {
        var comment = $(button).parent()

        $(comment).children("textarea").prop("disabled", false)
        $(comment).children("button.edit").hide()
        $(comment).children("button.save").show()
    },
    update: (button) => {
        var comment = $(button).parent()

        var id = $(comment).attr("id").replace("comment-","")
        var content = $(comment).children("textarea").val()

        $.ajax({
            type:"PATCH",
            url: "/postagens",
            data: {'content': content, 'id': id},
            success: (data) => {
                $(comment).children("textarea").prop("disabled", true)
                $(comment).children("button.edit").show()
                $(comment).children("button.save").hide()
            },
            error: () => {
                console.log("Ocorreu um erro")
            },
            dataType: 'json'
        })
    },

    remove: ( button ) => {
         var comment = $(button).parent()

        var id = $(comment).attr("id").replace("comment-","")

        $.ajax({
            type:"DELETE",
            url: "/postagens",
            data: { 'id': id},
            success: (data) => {
                $(comment).remove()
            },
            error: () => {
                console.log("Ocorreu um erro!")
            },
            dataType: 'json'
        })
    }
}

User = {
    findAll: () => {
        $.ajax({
            type: "GET",
            url: "/usuarios",
            success: User.loadAll,
            dataType: "json"
        })
    },

    loadAll: (data) => {
        var userCombo = $("#users")

        for(user of data) {
            userCombo.append($("<option></option>").attr("value", user.id).html(user.firstname + ' ' + user.lastname))
        }
    }
}

$(() => {
    User.findAll()

    Posts.findAll() 
})