Posts = {
    add : () => {
        var t = {}
        t.content = document.getElementById("content").value
        t.firstname = document.getElementById("firstname").value
        t.lastname = document.getElementById("lastname").value
        console.log(t)
        var xhttp = new XMLHttpRequest()

        xhttp.onreadystatechange = () => {
            console.log(xhttp.readyState)
            if(xhttp.readyState == 4 && xhttp.status == 200) {
                var resp = JSON.parse(xhttp.responseText)
                if(resp) {
                    console.log(resp)
                    Posts.template(resp)
                }
            }
        }
        xhttp.open("POST", "/postagens", true)
        xhttp.setRequestHeader("Content-Type", "application/json; charset=utf-8")
        xhttp.send(JSON.stringify(t))

        return false
    },

    template: (data) => {
        var comment = document.createElement("div")
        comment.setAttribute("id", "comment-" + data.id)
        comment.setAttribute("class", "comment")


        var content = document.createElement("p")
        content.setAttribute("class","content")
        content.innerHTML = data.content

        var user = document.createElement("p")
        user.innerHTML = "Por "+ data.user.firstname + " "+ data.user.lastname
        user.setAttribute("class","user")

        var date = document.createElement("span")
        date.setAttribute("class", "date")

        var dtCreation = new Date(data.createdAt)
        date.innerHTML =  "\t" + (dtCreation.getDate() < 10 ? "0" + dtCreation.getDate() : dtCreation.getDate()) + "/" + ((dtCreation.getMonth() + 1) > 10 ? "0" +dtCreation.getMonth() : dtCreation.getMonth()) + "/"  + dtCreation.getFullYear()

        user.append(date)

        comment.append(content)
        comment.append(user)

        var comments = document.getElementById("comments")
        comments.append(comment)
    },

    findAll: () => {
        $.ajax({
            type: "GET",
            url: "/postagens",
            success: (data) => {
                console.log(data)
                for( var post of data) {
                    Post.template(post)
                }
            },
            error: () => {
                console.log("Ocorreu um erro!")
            },
            dataType: 'json'
        })
    }
}

console.log("eai")

